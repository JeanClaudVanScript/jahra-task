//Подключаем модули галпа
const gulp = require('gulp');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const del = require('del');
const browserSync = require('browser-sync').create();

//Порядок подключения CSS файлов
const cssFiles = [
	'src/css/main.css'
];
const jsFiles = [
	'src/js/main.js'
];


//Таск на стили CSS
function styles(){
	//Шаблон для поиска файлов CSS
	//Все файлы по шаблону './src/css/**/*.css'
	return gulp.src(cssFiles)
	//Объединение файлов в один
	.pipe(concat('style.css'))
	//Добавление префиксов
	.pipe(autoprefixer({
		browsers: ['last 2 versions'],
		cascade: false
	}))
	.pipe(cleanCSS({
		level: 2
	}))


	//Выходная папка для стилей
	.pipe(gulp.dest('build/css'))
	.pipe(browserSync.stream())
};

function scripts(){
	//Шаблог для поиска файлов JS
	//Все файлы по шаблону 'src/js/**/*.js'
	return gulp.src(jsFiles)
	//Объединение файлов в один
	.pipe(concat('script.js'))
	//Вызов углифай
	.pipe(uglify())

	//Выходная папка для стилей
	.pipe(gulp.dest('build/js'))
	.pipe(browserSync.stream())
};
//удаление в папке билд
function clean(){
	return del(['build/*'])
};
//watch
function watch(){
	browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    //Слежка за CSS файлами и изменениями
    gulp.watch('src/css/**/*.css', styles)
    //Слежка за JS файлами и изменениями
    gulp.watch('src/js/**/*.js', scripts)
    //Слежка за html файлами и изменениями
    gulp.watch("./*.html").on('change', browserSync.reload);
}




//Таск вызывающий функцию styles
gulp.task('styles', styles);
//Таск вызывающий функцию scripts
gulp.task('scripts', scripts);

gulp.task('del', clean);
//Таск для отслеживания изменений
gulp.task('watch', watch);
//Таск для удаления файлов в билд и запуск styles + scripts
gulp.task('build', gulp.series(clean, gulp.parallel(styles, scripts)));

gulp.task('dev', gulp.series('build', 'watch'));